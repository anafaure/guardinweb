var request = require("request");
var Client = require('./../models/client');
var User = require('./../models/user');
var Contact = require('./../models/contact');



exports.registerClient = async (req, res)=>{
  
  var dato = req.body
  var dniIngresado = dato.dni
  var nroCuenta = dato.nroCliente
  var nroTelefono = dato.telefono
  var email = dato.email
  var info;
  //CONSUMO API PARA TRAER EL CLIENTE
  request.get({
    "headers": { "key": "UFJr9Yi8kn", "secret":"p0YF0C9Ab2Tl3bkJxJF6GF26qr5Z08gL" },
    "url": "http://guardiane.dyndns.org:3030/api/goteam/clients?NroCuenta="+nroCuenta}, 
    (error, response, body) => {
    if(error) {
        return console.dir(error);
    }
    else{
      info = JSON.parse(body)
      var dniAlmacenado = info.data[0].DNI
      /*CUANTA INFO NECESITO VERIFICAR PARA CARGAR A LA BASE??*/
      dniAlmacenado = dniAlmacenado.replace(/\./g,'')
      dniAlmacenado = dniAlmacenado.trim()
      dniIngresado = dniIngresado.replace(/\./g,'')
      
      if(dniIngresado === dniAlmacenado){
        var cadena = (Math.random() * (2 - 1) + 1).toString();
        var texto = cadena.substr(-6)
        request.get({"url":"http://servicio.smsmasivos.com.ar/enviar_sms.asp?api=1&usuario=WORKSYSTEM&clave=WOR544&tos="+nroTelefono+"&texto= Codigo para verificar cuenta de guardian web"+texto})
        
        res.status(200).send({dniAlmacenado, texto, nroCuenta,email})
          
      }
        
      else{
        res.status(404).send({error:'Verifique sus datos'})
      }
    }
    
  })
} 
 
exports.checkCode = async(req, res)=> {
  var data = req.body;
 
  if(data.nroCodeIngresado == data.nroCodeReal){
    request.get({
      "headers": { "key": "UFJr9Yi8kn", "secret":"p0YF0C9Ab2Tl3bkJxJF6GF26qr5Z08gL" },
      "url": "http://guardiane.dyndns.org:3030/api/goteam/clients?NroCuenta="+data.nroCuenta}, 
      (error, response, body) => {
      if(error) {
          return console.dir(error);
      }
      else{
        info = JSON.parse(body)
        var nroCuenta = info.data[0].NroCuenta
        newClient = new Client({
          nroCuenta: info.data[0].NroCuenta,
          dni: info.data[0].DNI.replace(/\./g,''),
          cuit: info.data[0].CUIT,
          telefono: info.data[0].Telefono,
          razonSocial: info.data[0].RazonSocial,
          nombreComercial: info.data[0].NombreComercial,
          domicilioFiscal: info.data[0].DomicilioFiscal,
          localidadFiscal: info.data[0].LocalidadFiscal,
          CPFiscal: info.data[0].CPFiscal,
          provinciaFiscal: info.data[0].ProvinciaFiscal,
          domicilioInstal: info.data[0].DomicilioInstal,
          localidadInstal: info.data[0].LocalidadInstal,
          CPInstal: info.data[0].CPInstal,
          provinciaInstal: info.data[0].ProvinciaInstal,
          condicionIVA: info.data[0].CondicionIVA
        })
        
        newClient.save((err, c) => {
            if (err) { 
              res.status(404).send({ error: "Error al guardar cliente"},console.log(err)) 
            }
            else {   
              console.log(c)
              res.send({nroCuenta})
            }
          })
        }
      })
    } 
    else{
        res.status(404).send({error:'Codigo no valido'})
        }
}
  
exports.addUser = async(req, res)=> {
  let info
  let saldo
  let data
  let d = req.body
  let nroCuenta = d.nroCuenta
  let client = await Client.findOne({ nroCuenta })
  let user = new User({
    nombreUser: d.user,
    clave: d.clave,
    email: d.email,
    cliente: client._id

  })
 user.save((err, user) => {
  
    if (err) {res.status(500).send({ message: "Error en la registración de usuario", error: err })
 }
    else {
      request.get({
        "headers": { "key": "UFJr9Yi8kn", "secret":"p0YF0C9Ab2Tl3bkJxJF6GF26qr5Z08gL" },
        "url": "http://guardiane.dyndns.org:3030/api/goteam/balances?NroCuenta="+nroCuenta}, 
        (error, response, body) => {
        if(error) {
            return console.dir(error);
        }
        else{
          request.get({
            "headers": { "key": "UFJr9Yi8kn", "secret":"p0YF0C9Ab2Tl3bkJxJF6GF26qr5Z08gL" },
            "url": "http://guardiane.dyndns.org:3030/api/goteam/bills?NroCuenta="+nroCuenta}, 
            (error, response, body2) => {
            if(error) {
              return console.dir(error);
            }
            else{
              info = JSON.parse(body)
              if(info.data== ''){
                saldo = 0
              }
              else{
                saldo = info.data[0].Saldo
              }
             
              data = JSON.parse(body2)
              req.session.loggedin = true;
              req.session.username = user.nombreUser;
              req.session.userCuenta = nroCuenta
              res.send({urlBase: `http://${req.get('host')}`, saldo, data})
             
            }
            
          })
          
        }
        
      })
      

    }
 })
}

exports.auth = async(req, res)=> {
  let saldo
  let data 
	var username = req.body.username;
	var password = req.body.pass;
  
  if (username && password) {
    User.findOne({ nombreUser:username}).
    populate('cliente').
    exec(function (err, user) {
      if (user) {
        
       //verifico que la clave ingresada sea correcta 
       if(user.clave === password){
        req.session.loggedin = true
        req.session.username = user.nombreUser
        req.session.userCuenta = user.cliente[0].nroCuenta
        request.get({
          "headers": { "key": "UFJr9Yi8kn", "secret":"p0YF0C9Ab2Tl3bkJxJF6GF26qr5Z08gL" },
          "url": "http://guardiane.dyndns.org:3030/api/goteam/bills?NroCuenta="+req.session.userCuenta}, 
          (error, response, body2) => {
          if(error) {
              return console.dir(error);
          }
          else{
            data = JSON.parse(body2)
            request.get({
              "headers": { "key": "UFJr9Yi8kn", "secret":"p0YF0C9Ab2Tl3bkJxJF6GF26qr5Z08gL" },
              "url": "http://guardiane.dyndns.org:3030/api/goteam/balances?NroCuenta="+req.session.userCuenta}, 
              (error, response, body) => {
              if(error) {
                  return console.dir(error);
              }
              else{
                info = JSON.parse(body)
                
                if(info.data== ''){
                  saldo = 0
                  
                }
                else{
                  saldo = info.data[0].Saldo
                }
                console.log("entra")
                res.render('index',{urlBase: `http://${req.get('host')}`, saldo, data, username})  
             }
          
           })//cierre de lista de facturas
       
         }
      
        })//cierre de datos clientes
        
      }
      else{
        res.render('login',{urlBase: `http://${req.get('host')}`,error:'Error en credenciales'})
      }
            
      }
      else {
        
        res.render('login',{urlBase: `http://${req.get('host')}`,error:'Error en credenciales'})
          
    }
  }) //cierro query
}
 
}

exports.getPdf = async (req, res) => {
  let factura  = req.params.factura
  
  request.get({
    "headers": { "key": "UFJr9Yi8kn", "secret":"p0YF0C9Ab2Tl3bkJxJF6GF26qr5Z08gL"},
    "url": "http://guardiane.dyndns.org:3030/api/goteam/pdf/"+factura},
    
    (error, response, resp) => {
    if(error) {
        return console.dir(error);
    }
    else{
      pdff = JSON.parse(resp)
      pdf = pdff.pdf
      res.send({pdf})
    }
    
  })
     
}

exports.logout = (req, res) => {
  req.session.destroy((err) => {
      if (err) { res.negotiate(err) }
      res.redirect('/')
  })
}


exports.index = async(req, res)=> {
  if (!req.session.loggedin){
    res.render('login')
  }
  else{
  let nroCuenta = req.session.userCuenta;
  var username = req.session.username
  console.log(nroCuenta)
  request.get({
    "headers": { "key": "UFJr9Yi8kn", "secret":"p0YF0C9Ab2Tl3bkJxJF6GF26qr5Z08gL" },
    "url": "http://guardiane.dyndns.org:3030/api/goteam/bills?NroCuenta="+nroCuenta}, 
    (error, response, body2) => {
    if(error) {
        return console.dir(error);
    }
    else{
      
      data = JSON.parse(body2)

      request.get({
        "headers": { "key": "UFJr9Yi8kn", "secret":"p0YF0C9Ab2Tl3bkJxJF6GF26qr5Z08gL" },
        "url": "http://guardiane.dyndns.org:3030/api/goteam/balances?NroCuenta="+nroCuenta}, 
        (error, response, body) => {
        if(error) {
            return console.dir(error);
        }
        else{
          info = JSON.parse(body)
          console.log(info.data+"saldo")
          
          if(info.data== ''){
            saldo = 0
          }
          else{
            saldo = info.data[0].Saldo
          }
          
          res.render('index',{urlBase: `http://${req.get('host')}`, saldo, data, username})
             
            }
            
          })
          
        }
        
      })
      

    }
}


exports.perfil = async(req, res) => {
  var username = req.session.username
  request.get({
    "headers": { "key": "UFJr9Yi8kn", "secret":"p0YF0C9Ab2Tl3bkJxJF6GF26qr5Z08gL" },
    "url": "http://guardiane.dyndns.org:3030/api/goteam/clients?NroCuenta="+req.session.userCuenta}, 
    (error, response, body) => {
    if(error) {
        return console.dir(error);
    }
    else{
      data = JSON.parse(body)
      console.log(data)
      res.render('perfil',{urlBase: `http://${req.get('host')}`, data, username})  
    }
    
  })
}
exports.updatePass = async(req, res) =>{
  var data = req.body
  console.log(req.body)
  var username = req.session.username
  let user = await User.findOne({nombreUser: username })
  console.log(user.clave)
  if(user.clave===data.passAnterior){
    User.findOneAndUpdate({ nombreUser: username }, {$set: { clave: data.passNueva } }, (err, updateuser) => {
      if (err) { console.log(err); res.send({ error: "Error en la actualización de datos" }) }
      if (!updateuser) {
        console.log("error update")
        res.send({ message: "No se encontró el usuario" })
      } else {
        console.log("ok update")
        res.send({message:'Clave modificada '})
      }
     })
  }
  else{
    res.send({ message: "La clave anterior no coincide" })
  }
}
