var baseUrl = window.location.origin

evalEmpty = obj => {
    let errs = []
    for (let i in obj) { if (obj.hasOwnProperty(i)) if (obj[i] === '') errs.push(i) }
    return errs
}

$('.generator-code .emptyField').hide()
$('.generator-code .form-code').hide()
$('.generator-code .form-user').hide()

$('.form-generate button').on('click', (e) => {
    $('.generator-code .emptyField').hide()
    e.preventDefault()
    let data = {}
    data.nroCliente = $('.form-generate input#nroCliente').val()
    data.dni = $('.form-generate input#dni').val()
    data.email = $('.form-generate input#email').val()
    data.telefono = $('.form-generate input#telefono').val()
    

    let err = evalEmpty(data)
    if (err.length > 0) $('.generator-code .emptyField').show()

    else {      
        fetch(baseUrl+'/register', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify( data )
        }).then(resp => { return resp.json() }).then(info => {
            if (info.dniAlmacenado == data.dni) {
                console.log(info)
                $('.generator-code .code-generated p').html()
                $('.generator-code .form-generate').hide()
                $('.form-code input#nroCuenta').val(info.nroCuenta)
                $('.form-user  input#email').val(info.email)
                $('.form-code input#nroCodeReal').val(info.texto)
                $('.generator-code .form-code').show()
                
            } else {
                console.log(info)
                alert(info.error)
               
            }
            
        })
    }
})

$('.form-code button').on('click', (e) => {
    $('.generator-code .emptyField').hide()
    e.preventDefault()
    let data = {}
    data.nroCuenta = $('.form-code input#nroCuenta').val()
    data.nroCodeIngresado = $('.form-code input#nroCode').val()
    data.nroCodeReal = $('.form-code input#nroCodeReal').val()
    
    let err = evalEmpty(nroCode)
    if (err.length > 0) $('.generator-code .emptyField').show()
    
    else {      
        fetch(baseUrl+'/register/checkCode', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data)
        }).then(resp => { return resp.json() }).then(info => {
            
            if (info.error) {
                alert(info.error)
                
            } else {
                console.log(info.nroCuenta)
                $('.generator-code .form-generate').hide()
                $('.generator-code .form-code').hide()
                $('.generator-code .form-user').show()
                $('.form-user input#nroCuenta').val(info.nroCuenta) 
                
               
            }
        })
    }
})
$('.form-user button').on('click', (e) => {
    $('.generator-code .emptyField').hide()
    e.preventDefault()
    let data = {}
    if($('.form-user input#clave').val()===$('.form-user input#claveVerif').val()){
        data.clave = $('.form-user input#clave').val()
        data.claveV = $('.form-user input#claveVerif').val()
        data.user = $('.form-user input#user').val()
        data.nroCuenta = $('.form-user input#nroCuenta').val()
        data.email = $('.form-user input#email').val()
        let err = evalEmpty(data)
    if (err.length > 0){
         $('.generator-code .emptyField').show()
        }
    else {      
        fetch(baseUrl+'/addUser', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify( data )
        }).then(resp => { return resp.json() }).then(info => {
            console.log(info.saldo)
            var saldo = info.saldo
            var urlBase = info.urlBase
            var data = info.data
            window.location.replace(baseUrl+'/index',{urlBase, saldo, data})
        })   
        
      }
    }
    else {
         alert("verifique datos")
               
        }
   
})

function getPdf(id){
    var idFactura= id
   
    $("#modalEspera").fadeIn("fast")
    
    fetch(baseUrl+'/pdf/'+idFactura).then(resp => { return resp.json() }).then(async data => {
       window.location.assign(data.pdf)
    })
    
}


  $( '#basic-checkbox1' ).on('click', function() {
      
    if( $(this).is(':checked') ){
        var info = $(this).val()
        console.log(info+"valorcbu")
        // Hacer algo si el checkbox ha sido seleccionado
        document.getElementById('is').style.display = "block";
        document.getElementById('ic').style.display = "none";
        $('.form-debitoCbu input#tipo').val(info)
        $("#basic-checkbox4").prop("checked", false);
        $("#basic-checkbox2").prop("checked", false);
        $("#basic-checkbox3").prop("checked", false);
        }
        
  });
  $( '#basic-checkbox2' ).on('click', function() {
    
  if( $(this).is(':checked') ){
      var info = $(this).val()
      console.log(info+"valorcbu")
      // Hacer algo si el checkbox ha sido seleccionado
      document.getElementById('ic').style.display = "block";
      document.getElementById('is').style.display = "none";
      $('.form-debitoTarjeta input#tipo').val(info)
      $("#basic-checkbox1").prop("checked", false);
      $("#basic-checkbox4").prop("checked", false);
      $("#basic-checkbox3").prop("checked", false);
      }
      
});
$( '#basic-checkbox3' ).on('click', function() {
    console.log("emtra")
  if( $(this).is(':checked') ){
      var info = $(this).val()
      console.log(info+"valorcbu")
      // Hacer algo si el checkbox ha sido seleccionado
      document.getElementById('ic').style.display = "block";
      document.getElementById('is').style.display = "none";
      $('.form-debitoTarjeta input#tipo').val(info)
      $("#basic-checkbox1").prop("checked", false);
      $("#basic-checkbox2").prop("checked", false);
      $("#basic-checkbox4").prop("checked", false);
      }
      
});

$( '#basic-checkbox4' ).on('click', function() {
    console.log("emtra")
  if( $(this).is(':checked') ){
      var info = $(this).val()
      console.log(info+"valorcbu")
      // Hacer algo si el checkbox ha sido seleccionado
      document.getElementById('ic').style.display = "block";
      document.getElementById('is').style.display = "none";
      $('.form-debitoTarjeta input#tipo').val(info)
      $("#basic-checkbox1").prop("checked", false);
      $("#basic-checkbox2").prop("checked", false);
      $("#basic-checkbox3").prop("checked", false);
      }
      
});

$( '#buttonPass' ).on('click', function() {
    console.log("emtra")
    document.getElementById('pass').style.display = "block";
      
})

$('.form-updatePass button').on('click', (e) => {
    let data = {}
    
    if($('.form-updatePass input#passNueva').val()===$('.form-updatePass input#passNuevaVerif').val()){
        data.passAnterior = $('.form-updatePass input#passAnterior').val()
        data.passNueva = $('.form-updatePass input#passNueva').val()
         console.log("llega")     
        fetch(baseUrl+'/updatePass', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify( data )
        }).then(resp => { return resp.json() }).then(info => {
            console.log(info.message)
            alert(info.message)
            window.location.replace(baseUrl+'/perfil')
            
        })   
        
      }
      else {
        alert("Verifique nueva clave")
        window.location.replace(baseUrl+'/perfil')
      }
    })



