var mongoose = require('mongoose');
  var Schema = mongoose.Schema;

  var ContactSchema = new Schema({
    nombre: String,
    celular: Number,
    llave: Boolean,
    palabraClave: String,
    orden: Number, 
    verificado: Boolean,
    idRemoto: String,
    estado: Boolean,
    cliente: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Client' }]
  }, { timestamps: true });
module.exports = mongoose.model('Contact', ContactSchema)