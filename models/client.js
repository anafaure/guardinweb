var mongoose = require('mongoose');
  Schema = mongoose.Schema;

  ClientSchema = new Schema({
    nroCuenta: { type: Number, unique:true },
    dni: Number,
    cuit:  String,
    telefono: String,
    razonSocial: String,
    nombreComercial: String,
    domicilioFiscal: String,
    localidadFiscal: String,
    CPFiscal: Number,
    provinciaFiscal: String,
    domicilioInstal: String,
    localidadInstal: String,
    CPInstal: Number,
    provinciaInstal: String,
    condicionIVA: String
  }, { timestamps: true });

module.exports = mongoose.model('Client', ClientSchema)