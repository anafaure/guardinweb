var express = require('express');
var router = express.Router();
var ctrlUser = require('../controller/user');
var ctrlContact = require('../controller/contact');
var ctrlFactura = require('../controller/factura');

/* GET home page. */
router.get('/', function (req, res) {
  req.session.loggedin = false
  console.log(req.session.loggedin)
  res.render('login',{urlBase: `http://${req.get('host')}`, error:""});
});

router.get('/index', ctrlUser.index)
router.get('/newDebito',function(req, res) {
  res.render('newDebito',{urlBase: `http://${req.get('host')}`, username:req.session.username});
});
 
router.post('/addUser', ctrlUser.addUser)
router.post('/auth', ctrlUser.auth)
router.get('/logout', ctrlUser.logout)
router.get('/pdf/:factura', ctrlUser.getPdf)
router.get('/contact', ctrlContact.listContact) 
router.post('/newContact', ctrlContact.newContact)
router.get('/getCodeContact/:id', ctrlContact.getCodeContact)
router.post('/checkCodeContact', ctrlContact.checkCodeContact)
router.get('/getContact/:id', ctrlContact.getContact)
router.get('/deleteContact/:idContact', ctrlContact.deleteContact)
router.post('/updateContact', ctrlContact.updateContact)
router.post('/updateContectOrden', ctrlContact.updateContactOrden)
router.post('/addDebito', ctrlFactura.addDebito)
router.get('/perfil', ctrlUser.perfil)
router.post('/updatePass', ctrlUser.updatePass) 

module.exports = router;
