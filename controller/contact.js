var request = require("request");
var Contact = require('./../models/contact');
var User = require('./../models/user');
var Client = require('./../models/client');
var nodemailer = require('nodemailer');


exports.newContact = async (req, res) => {
    
    var client = await Client.findOne({ nroCuenta: req.session.userCuenta})
    var nombre  = req.body.nombre
    var celular = req.body.celular
    var palabraClave = req.body.palabraClave
    var llave = req.body.llave
    
    if(llave === 'on'){
        llave = true
    }
    else{
        llave = false
    }

    newContact = new Contact({
        nombre: nombre,
        celular: celular,
        llave: llave,
        palabraClave: palabraClave,
        orden: 1, 
        verificado: false,
        idRemoto: "",
        estado: true,
        cliente: client._id 
        })
        newContact.save((err, contact) => {

            if (err) {
                res.status(500).send({ message: "Error en la registración de contacto", error: err })
            }
            else {
                console.log(contact)

            }
         })  
         res.redirect('contact')
                   
}

exports.getCodeContact = async (req, res) => {
      
    var id = req.params.id
    var contact = await Contact.findOne({ _id: id})
    var cadena = (Math.random() * (2 - 1) + 1).toString();
    var texto = cadena.substr(-6)
    request.get({"url":"http://servicio.smsmasivos.com.ar/enviar_sms.asp?api=1&usuario=WORKSYSTEM&clave=WOR544&tos="+contact.celular+"&texto="+texto})
    console.log(texto)
    res.send({texto, id})
}

exports.checkCodeContact = async(req, res)=> {
    var data = req.body
    var contact = await Contact.findOne({_id: data.nroId})
    var client = await Client.findOne({ nroCuenta: req.session.userCuenta})
        if(data.nroCodeIngresado === data.nroCodeReal){
            var nroCuenta=req.session.userCuenta.toString()
            var telCel =contact.celular.toString()
            var user = req.session.username
            const dato = JSON.stringify({
                "NroCuenta": nroCuenta,
                            
                "Nombre": contact.nombre,
                            
                "Clave": contact.palabraClave,
                            
                "Llave": contact.llave,
                            
                "TelCel": telCel,
                            
                "Orden": 1,
                            
                "Vigencia": "2019-10-10",
                            
                "Origen": {
                            
                    "Usuario": user,
                            
                    "IP": "192.168.0.10",
                            
                    "NombreCompleto": client.nombreComercial
                            
                }
                            
            })
            
            request.post({
                "headers": { "key": "UFJr9Yi8kn", "secret":"p0YF0C9Ab2Tl3bkJxJF6GF26qr5Z08gL","Content-Type": "application/json"},
                "url": "http://guardiane.dyndns.org:3030/api/goteam/contacts/",
                "body": dato },
                        
                (error, response, resp) => {
                if(error) {
                    return console.dir(error);
                }
                else{
                    respuesta = JSON.parse(resp)
                    console.log(respuesta)
            
            Contact.findOneAndUpdate({ _id: data.nroId }, {$set: { verificado: true, idRemoto: respuesta.ID } }, (err, cli) => {
                if (err) { console.log(err); res.send({ error: "Error en la actualización de datos" }) }
                if (!cli) {
                    res.send({ error: "No se encontró el contacto" })
                } else {
                    res.send({message:'Se verifico el contacto '})
                }
                        
            })
                   
        }
    })
             
    } 
    else{
        res.send({error:'Codigo no valido'})
    }
}

exports.getContact = async (req, res) => {
      
   var id = req.params.id
   var contact = await Contact.findOne({ _id: id})
   /*request.get({
    "headers": { "key": "UFJr9Yi8kn", "secret":"p0YF0C9Ab2Tl3bkJxJF6GF26qr5Z08gL","Content-Type": "application/json"},
    "url": "http://guardiane.dyndns.org:3030/api/goteam/contacts/"+id },
    
    (error, response, resp) => {
    if(error) {
        return console.dir(error);
    }
    else{
        contact = JSON.parse(resp)
        console.log(contact)
        res.send({contact})
                    
    }
    
  })*/
  console.log(contact)
  res.send({contact})
}      

exports.updateContact = async(req, res)=> {
    var data = req.body
    var client = await Client.findOne({ nroCuenta: req.session.userCuenta})
    var nroCuenta = req.session.userCuenta.toString()
    var user = req.session.username
    if (data.llave=='no'){
        data.llave = false
    }
    else if(data.llave=='NO'){
        data.llave = false
    }
    else{
        data.llave = true
    }
    console.log(data.llave)
    Contact.findOneAndUpdate({ _id: data.id }, {$set: { nombre: data.nombre, celular: data.celular, palabraClave: data.palabraClave, llave:data.llave } }, (err, cli) => {
        if (err) { console.log(err); res.send({ error: "Error en la actualización de datos" }) }
            if (!cli) {
                res.send({ error: "No se encontró el cliente" })
            } else {
                if (cli.verificado === true){
                    /*if(cli.llave == 'NO'){
                        cli.llave = false
                    }
                    if (data.llave=='no'){
                        data.llave = false
                    }
                   else{
                    cli.llave = true
                   }*/
                   const info = JSON.stringify({
                        
                    "NroCuenta": nroCuenta,
                    
                    "Nombre": data.nombre,
                    
                    "Clave": data.palabraClave,
                    
                    "Llave": data.llave,
                    
                    "TelCel": data.celular.toString(),
                    
                    "Orden": parseInt(cli.orden),
                    
                    "Vigencia": "2019-12-31",
                    
                    "Origen": {
                    
                        "Usuario": user,
                    
                        "IP": "192.168.0.10",
                    
                        "NombreCompleto": client.nombreComercial
                    
                        }
                    
                    })
                
                   request.put({
                    "headers": { "key": "UFJr9Yi8kn", "secret":"p0YF0C9Ab2Tl3bkJxJF6GF26qr5Z08gL","Content-Type": "application/json"},
                    "url": "http://guardiane.dyndns.org:3030/api/goteam/contacts/"+cli.idRemoto,
                    "body": info },
                    
                    (error, response, resp) => {
                    if(error) {
                        return console.dir(error);
                    }
                    else{
                      respuesta = JSON.parse(resp)
                      console.log(respuesta)
                                    
                      res.redirect('contact')
                    }
                    
                     })
                     
                }
                else{
                    res.redirect('contact')
                }
                
            }
        })
    
   
  }

exports.deleteContact = async(req, res)=> {
    var id =req.params.idContact
    let user = await User.findOne({ nombreUser: req.session.username })
    console.log(user)
                        
    Contact.findOneAndUpdate({ _id: id }, {$set: { estado: false } }, (err, cli) => {
        if (err) { console.log(err); res.send({ error: "Error en la actualización de datos" }) }
            if (!cli) {
                res.send({ error: "No se encontró el contacto" })
            } else {
                request.delete({
                    "headers": { "key": "UFJr9Yi8kn", "secret":"p0YF0C9Ab2Tl3bkJxJF6GF26qr5Z08gL","Content-Type": "application/json"},
                    "url": "http://guardiane.dyndns.org:3030/api/goteam/contacts/"+cli.idRemoto },
                    
                    (error, response, resp) => {
                    if(error) {
                        return console.dir(error);
                    }
                    else{
                        respuesta = JSON.parse(resp)
                        //Creamos el objeto de transporte
                        var transporter = nodemailer.createTransport({
                            service: 'Gmail',
                            auth: {
                            user: 'analia.f93@gmail.com',
                            pass: '22demayo'
                            }
                        });
                        var email="Se acaba de eliminar un contacto de tu lista";
                        var mailOptions = {
                            from: 'GuardianWeb',
                            to: user.email,
                            subject: 'Movimientos en guardianWeb',
                            text: email
                        };
                        transporter.sendMail(mailOptions, function(error, info){
                            if (error) {
                            console.log(error);
                            } else {
                            console.log('Email enviado: ' + info.response);
                            }
                        });
                        res.send({info:"ok"})
                    }
                    
                })
            }
        })
}

  exports.listContact = async (req, res) => {
    var username = req.session.username
    if( req.session.username ){
        let client = await Client.findOne({ nroCuenta: req.session.userCuenta })
        let data = await Contact.find({cliente: client._id, estado: true}).sort('orden')
        res.render('contact',{data,username})
         
    }
 }
 
 exports.updateContactOrden = async (req, res) => {
    if( req.session.username ){
    let user = await User.findOne({ nombreUser: req.session.username})
    let client = await Client.findOne({ nroCuenta: req.session.userCuenta })
    var config = req.body
     
    for(const x in config){
        let cont = await Contact.findOneAndUpdate({cliente: client._id, estado: true, nombre:config[x] }, { orden: x }, {
            new: true
        })
        
     }
     
     //Creamos el objeto de transporte
     var transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
        user: 'analia.f93@gmail.com',
        pass: '22demayo'
        }
    });
    var email="Se modifico el orden de prioridad en tu lista de contactos";
    var mailOptions = {
        from: 'GuardianWeb',
        to: user.email,
        subject: 'Movimientos en guardianWeb',
        text: email
    };
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
        console.log(error);
        } else {
        console.log('Email enviado: ' + info.response);
        }
    });
    res.send({message:'Orden de contactos guardado!'})
    }
 }
   


