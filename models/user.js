var mongoose = require('mongoose');
  var Schema = mongoose.Schema;

  var UserSchema = new Schema({
    nombreUser: { type: String, unique: true},
    clave: String,
    email: String,
    cliente: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Client' }]
  }, { timestamps: true });
module.exports = mongoose.model('User', UserSchema)