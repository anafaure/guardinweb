var baseUrl = window.location.origin
var almacenaId = {}
var ordenItem = []
//div con efecto para avisos
$("#aviso").fadeIn("fast")
$("#aviso").fadeOut(2000)
//////////////////////////
$( function() {
  $( "#sortable" ).sortable({
    stop: function(event){
    var count = document.getElementById('lala').rows.length -1
    
    for(var i=0; i < count; i++){
      almacenaId[i] = $('#nombre').val()
      ordenItem[i]= i+1
      console.log($('#nombre').val())
    }
    
    console.log(count)
    }
  })
  $( "#sortable" ).disableSelection()
} )

function guardanOrden(){
  var i = 0
  $('#lala th').each(function() { 
   almacenaId[i] = $(this).html()
   i++
  })
  console.log(almacenaId)
  fetch(baseUrl+'/updateContectOrden', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(almacenaId)
  }).then(resp => { return resp.json() }).then(info => {
       alert(info.message)
        window.location.replace(baseUrl+'/contact')
    
})

}
function showModalDelete(idContact){
  $( "input#id.input100" ).val(idContact)
  document.getElementById('openModalDelete').style.display = 'block';
}
function CloseModalDelete() {
  document.getElementById('openModalDelete').style.display = 'none';
}
function deleteContact(){
  var idContact = $(" #id").val()
  console.log(idContact)
  fetch(baseUrl+'/deleteContact/'+idContact).then(resp => { return resp.json() }).then(async data => {
  
        console.log(data)
        window.location.replace(baseUrl+'/contact')
  })
}

function showModal(idContact) {
  
  fetch(baseUrl+'/getContact/'+idContact).then(resp => { return resp.json() }).then(async data => {
    
    $('.form-updateContact input#id').val(idContact)
    $('.form-updateContact input#nombre').val(data.contact.nombre)
    $('.form-updateContact input#celular').val(data.contact.celular)
    $('.form-updateContact input#palabraClave').val(data.contact.palabraClave)
    
    if(data.contact.llave===true){
      $('.form-updateContact input#llave').val('SI')
    
    }
    else{
      $('.form-updateContact input#llave').val('NO')
    
    }
   
  })
    document.getElementById('openModal').style.display = 'block';
  }

  
  function CloseModal() {
    document.getElementById('openModal').style.display = 'none';
  }

function newContact(){
  document.getElementById('openModal2').style.display = 'block';
}
function newContactClose(){
  document.getElementById('openModal2').style.display = 'none';
}

function checkContact(){
  var idContact= $("#checkContact").attr("name")

  fetch(baseUrl+'/getCodeContact/'+idContact).then(resp => { return resp.json() }).then(async data => {
    $('.form-checkContact input#nroCode').val(data.texto)
    $('.form-checkContact input#nroId').val(data.id)
    console.log(data)
  })
  document.getElementById('openCheckContact').style.display = 'block';
}
function checkContactClose(){
  document.getElementById('openCheckContact').style.display = 'none';
}

$('.form-checkContact button').on('click', (e) => {
 
  //$('.generator-code .emptyField').hide()
  e.preventDefault()
  let data = {}
  data.nroId = $('.form-checkContact input#nroId').val()
  data.nroCodeIngresado = $('.form-checkContact input#nroCode').val()
  data.nroCodeReal = $('.form-checkContact input#nroCodeCheck').val()
  console.log(data)
      fetch(baseUrl+'/checkCodeContact', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(data)
      }).then(resp => { return resp.json() }).then(info => {
          
          if (info.error) {
              alert(info.error)
              window.location.replace(baseUrl+'/contact')
              
          } else {
              
              window.location.replace(baseUrl+'/contact')
              alert(info.message)
            
          }
      })
  
})


