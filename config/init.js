let config = {
    port: 3000,
    session: {
        secretPass: 'Gu4rW3b',
        maxAge: 36000000
    },
    pathDatabase: 'mongodb://localhost:27017/guardianWeb',
    encrypt: 'Gu4rW3bEncryp'
}

module.exports = config